<?php

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

$databases = array();
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'u537976954_eq',
  'username' => 'u537976954_root',
  'password' => 'HSMk7dMJVZ4r',
  'host' => 'localhost',
  'prefix' => '',
  'collation' => 'utf8_general_ci',
);

if (isset($_SERVER['ARCHIE_INSTANCE'])) {
  // DB Array and some common conf.
  $databases['default']['default'] = array(
    'driver' => 'mysql',
    'database' => 'eq_new',
    'username' => 'root',
    'password' => '',
    'host' => 'localhost',
    'port' => '3306',
    'prefix' => '',
  );
}

$update_free_access = FALSE;
$drupal_hash_salt = 'wergw54545';

$base_url = 'http://eazyquest.net';
if (isset($_SERVER['ARCHIE_INSTANCE'])) {
  $base_url = 'http://eq.dev';
}

