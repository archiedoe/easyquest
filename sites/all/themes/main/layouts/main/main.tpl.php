<div class="main-wrapper">
  <div class="main-header-wrapper">
    <div class="main-header">
      <?php print $content['header']; ?>
    </div>
  </div>

  <div class="main-content-wrapper">
    <div class="main-content">
      <div class="left">
        <?php print $content['content_left']; ?>
      </div>
      <div class="right">
        <?php print $content['content_right']; ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="main-footer-wrapper">
    <div class="main-footer">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>