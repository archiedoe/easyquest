<?php

$plugin = array(
  'title' => t('Main'),
  'category' => t('Custom'),
  'theme' => 'main',
  'icon' => 'onecol.png',
  'css' => 'main.css',
  'regions' => array(
    'header' => t('Header'),
    'content_left' => t('Content left'),
    'content_right' => t('Content right'),
    'footer' => t('Footer'),
  ),
);
