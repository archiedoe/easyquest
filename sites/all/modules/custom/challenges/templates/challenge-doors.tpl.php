<div class="challenge-doors">
  <table class="map">
    <?php $out1 = ''; ?>
    <?php for ($i = 0; $i < CHALLENGE_DOORS_PERIMETER_X; $i++): ?>
      <?php $dat = '<tr>'; ?>
        <?php for ($j = 0; $j < CHALLENGE_DOORS_PERIMETER_Y; $j++): ?>
          <?php $dat .= '<td class="' . ($data[$j][$i][CH_CON_VS] ? 'visited' : '') . ($j == $data['c_door'][0] && $i == $data['c_door'][1] ? ' current cur-' . $data['c_dir'] : '') . '"></td>'; ?>
        <?php endfor; ?>
      <?php $dat .= '</tr>'; ?>
      <?php $out1 = $dat . $out1; ?>
    <?php endfor; ?>
    <?php print $out1; ?>
  </table>
  <div class="wall">
    <div class="wall-inner">
      <?php if ($out['left']): ?>
        <a class="door-left" href="/challenge/doors?dir=3"></a>
      <?php endif; ?>
      <?php if ($out['right']): ?>
        <a class="door-right" href="/challenge/doors?dir=1"></a>
      <?php endif; ?>
      <?php if ($out['top']): ?>
        <a class="door-top" href="/challenge/doors?dir=0"></a>
      <?php endif; ?>
      <?php if ($out['bottom']): ?>
        <a class="door-bottom" href="/challenge/doors?dir=2">Развернуться и выйти</a>
      <?php endif; ?>
      <?php if ($out['won']): ?>
        <h1>Вы выиграли!</h1>
      <?php endif; ?>
    </div>
  </div>
</div>