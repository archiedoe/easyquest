<?php

define('CHALLENGE_CH_DOORS_ID', 2);
define('CHALLENGE_DOORS_PERIMETER_X', 8);
define('CHALLENGE_DOORS_PERIMETER_Y', 8);
define('CHALLENGE_DOORS_EXIT_MIN_X', 5);
define('CHALLENGE_DOORS_EXIT_MIN_Y', 5);
define('CHALLANGE_MIN_ROOMS_COUNT', 20);


// Const type
define('CH_CON_TP', 0);

// Const Type usual
define('CH_CON_TP_V_USL', 0);
// Const Type won
define('CH_CON_TP_V_WON', 1);
// Const Type trap
define('CH_CON_TP_V_TRP', 2);
// Const Type dead end
define('CH_CON_TP_V_DE', 3);



// Const visited
define('CH_CON_VS', 1);

// Const visited visited
define('CH_CON_VS_TR', TRUE);
// Const visited not visited
define('CH_CON_VS_FL', FALSE);



// Const path
define('CH_CON_PATH', 2);

// Const path top
define('CH_CON_PATH_TOP', 0);
// Const path right
define('CH_CON_PATH_RGH', 1);
// Const path bot
define('CH_CON_PATH_BOT', 2);
// Const path left
define('CH_CON_PATH_LFT', 3);


define('CH_DIR_N', 0);
define('CH_DIR_E', 1);
define('CH_DIR_S', 2);
define('CH_DIR_W', 3);


function challenge_challenge_doors() {
  global $user;
  
  $lvisited = FALSE;
  $rvisited = FALSE;
  $last = FALSE;
  $won = FALSE;
  $award = NULL;
  
  $raw_data = challenges_data_load(CHALLENGE_CH_DOORS_ID, $user->uid);
  
  $data = unserialize($raw_data->data);
  
  if (empty($data)) {
    $data = challange_generate_challenge();
    $data['c_door'] = array(0, 0);
    $data['c_dir'] = CH_DIR_N;
    $data[0][0][CH_CON_VS] = TRUE;

    $full_data = array('uid' => $user->uid, 'type' => CHALLENGE_CH_DOORS_ID, 'data' => serialize($data), 'date' => 0);
    drupal_write_record('challenges', $full_data);
  }
  
  if (isset($_GET['dir'])) {
    
    $way_map = challenge_get_way_map_real($data['c_dir']);
    if (isset($data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_PATH][$way_map[$_GET['dir']]])) {
      
      $way_map = challenge_get_way_map_real($data['c_dir']);
      
      $data['c_dir'] += $_GET['dir'];
      if ($data['c_dir'] > 3) {
        $data['c_dir'] -= 4;
      }
      
      $change = array(
        CH_CON_PATH_TOP => array(0, 1),
        CH_CON_PATH_RGH => array(1, 0),
        CH_CON_PATH_BOT => array(0, -1),
        CH_CON_PATH_LFT => array(-1, 0)
      );
      
      $ch = $change[$way_map[$_GET['dir']]];
      
      $data['c_door'][0] += $ch[0];
      $data['c_door'][1] += $ch[1];
      $data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_VS] = TRUE;
    }
  }
  
  $way_map = challenge_get_way_map_real($data['c_dir']);
  $out['left'] = isset($data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_PATH][$way_map[CH_CON_PATH_LFT]]);
  $out['right'] = isset($data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_PATH][$way_map[CH_CON_PATH_RGH]]);
  $out['top'] = isset($data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_PATH][$way_map[CH_CON_PATH_TOP]]);
  $out['bottom'] = isset($data[$data['c_door'][0]][$data['c_door'][1]][CH_CON_PATH][$way_map[CH_CON_PATH_BOT]]);
  
  if ($data['won'][0] == $data['c_door'][0] && $data['won'][1] == $data['c_door'][1]) {
    $out['won'] = TRUE;
  }

  $full_data = array('cid' => $raw_data->cid, 'uid' => $user->uid, 'type' => CHALLENGE_CH_DOORS_ID, 'data' => serialize($data), 'date' => 0);
  challenges_data_update($full_data);
  
  
  return theme('challenge_doors', array('out' => $out, 'data' => $data));
}

function challange_generate_challenge() {
  $data = array();
  
  $exit_x = mt_rand(CHALLENGE_DOORS_EXIT_MIN_X, CHALLENGE_DOORS_PERIMETER_X - 1);
  $exit_y = mt_rand(CHALLENGE_DOORS_EXIT_MIN_Y, CHALLENGE_DOORS_PERIMETER_Y - 1);
  
  $data['generated'] = FALSE;
  while (!$data['generated']) {
    $data = array();
    
    print $data['generated'];
    $data['won'] = array($exit_x, $exit_y);
    $data['count'] = 0;
    
    $data[0][0] = array(
      CH_CON_TP => CH_CON_TP_V_USL,
      CH_CON_VS => CH_CON_VS_FL,
    );
    
    challenge_gen_iteration($data, 0, 0);
  }
  
  return $data;
}

function challenge_gen_iteration(&$data, $prev_x = -1, $prev_y = -1) {
  // Determining next step
  
  // First checking which way to go.
  $way = mt_rand(0, 3);
  $change = array(
    CH_CON_PATH_TOP => array(0, 1),
    CH_CON_PATH_RGH => array(1, 0),
    CH_CON_PATH_BOT => array(0, -1),
    CH_CON_PATH_LFT => array(-1, 0)
  );
  
  $i = 4;
  while ($i--) {
    // Checking if we can move that way
    $new_x = $prev_x + $change[$way][0];
    $new_y = $prev_y + $change[$way][1];
    
    if ($new_x >= 0 && $new_y >= 0 && $new_x < CHALLENGE_DOORS_PERIMETER_X && $new_y < CHALLENGE_DOORS_PERIMETER_Y && !isset($data[$new_x][$new_y])) {
      $data['count']++;
      $data[$prev_x][$prev_y][CH_CON_PATH][$way] = TRUE;
      
      $data[$new_x][$new_y] = array(
        CH_CON_VS => CH_CON_VS_FL,
      );
      
      $op_way = challenge_doors_get_way_negative($way);
      $data[$new_x][$new_y][CH_CON_PATH][$op_way] = TRUE;
      
      // Checking if this path is deadend
      $is_de = mt_rand(1, 10);
      
      if ($is_de < 2) {
        $data[$new_x][$new_y][CH_CON_TP] = CH_CON_TP_V_DE;
      }
      elseif ($data['won'][0] == $new_x && $data['won'][1] == $new_y) {
        $data[$new_x][$new_y][CH_CON_TP] = CH_CON_TP_V_WON;
        $data['generated'] = TRUE;
      }
      else {
        $data[$new_x][$new_y][CH_CON_TP] = CH_CON_TP_V_USL;
      }
      
      if ($data[$new_x][$new_y][CH_CON_TP] == CH_CON_TP_V_USL && !($data['generated'] && $data['count'] > CHALLANGE_MIN_ROOMS_COUNT)) {
        challenge_gen_iteration($data, $new_x, $new_y);
      }
    }
    
    $way += $way < 3 ? 1 : -3;
  }
}

function challenge_get_way_map($dir) {
  if ($dir == CH_DIR_N) {
    return array(
      'lft' => CH_CON_PATH_LFT,
      'rgh' => CH_CON_PATH_RGH,
      'top' => CH_CON_PATH_TOP,
      'bot' => CH_CON_PATH_BOT,
    );
  }
  elseif ($dir == CH_DIR_E) {
    return array(
      'lft' => CH_CON_PATH_TOP,
      'rgh' => CH_CON_PATH_BOT,
      'top' => CH_CON_PATH_RGH,
      'bot' => CH_CON_PATH_LFT,
    );
  }
  elseif ($dir == CH_DIR_S) {
    return array(
      'lft' => CH_CON_PATH_RGH,
      'rgh' => CH_CON_PATH_LFT,
      'top' => CH_CON_PATH_BOT,
      'bot' => CH_CON_PATH_TOP,
    );
  }
  elseif ($dir == CH_DIR_W) {
    return array(
      'lft' => CH_CON_PATH_BOT,
      'rgh' => CH_CON_PATH_TOP,
      'top' => CH_CON_PATH_LFT,
      'bot' => CH_CON_PATH_RGH,
    );
  }
}

function challenge_get_way_map_real($dir) {
  if ($dir == CH_DIR_N) {
    return array(
      CH_CON_PATH_LFT => CH_CON_PATH_LFT,
      CH_CON_PATH_RGH => CH_CON_PATH_RGH,
      CH_CON_PATH_TOP => CH_CON_PATH_TOP,
      CH_CON_PATH_BOT => CH_CON_PATH_BOT,
    );
  }
  elseif ($dir == CH_DIR_E) {
    return array(
      CH_CON_PATH_LFT => CH_CON_PATH_TOP,
      CH_CON_PATH_RGH => CH_CON_PATH_BOT,
      CH_CON_PATH_TOP => CH_CON_PATH_RGH,
      CH_CON_PATH_BOT => CH_CON_PATH_LFT,
    );
  }
  elseif ($dir == CH_DIR_S) {
    return array(
      CH_CON_PATH_LFT => CH_CON_PATH_RGH,
      CH_CON_PATH_RGH => CH_CON_PATH_LFT,
      CH_CON_PATH_TOP => CH_CON_PATH_BOT,
      CH_CON_PATH_BOT => CH_CON_PATH_TOP,
    );
  }
  elseif ($dir == CH_DIR_W) {
    return array(
      CH_CON_PATH_LFT => CH_CON_PATH_BOT,
      CH_CON_PATH_RGH => CH_CON_PATH_TOP,
      CH_CON_PATH_TOP => CH_CON_PATH_LFT,
      CH_CON_PATH_BOT => CH_CON_PATH_RGH,
    );
  }
}

function challenge_doors_get_way_negative($way) {
  if ($way == CH_CON_PATH_TOP) {
    return CH_CON_PATH_BOT;
  }
  if ($way == CH_CON_PATH_BOT) {
    return CH_CON_PATH_TOP;
  }
  if ($way == CH_CON_PATH_LFT) {
    return CH_CON_PATH_RGH;
  }
  if ($way == CH_CON_PATH_RGH) {
    return CH_CON_PATH_LFT;
  }
}