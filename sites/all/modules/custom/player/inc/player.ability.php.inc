<?php

/**
 * Main ability class
 */
class Ability {
  
  // Ability id
  public $id = 0;
  
  // Ability school
  public $school;
  
  // Ability type
  public $type = '';
   
  // Ability title
  public $title = '';
  
  // Ability icon
  public $icon = '';
  
  // Ability cooldown
  public $cooldown = 0;
  
  // Ability callback
  private $callback = '';
  
  // Flag if ability is active (most used for buffs or debuffs)
  private $active = FALSE;
  
  // Ability data 
  private $data = array();
  
  // Keeps how many more steps this ability will be active
  private $duration = 0;
  
  // Ability arguments - data required to be collected by player
  private $arguments = array();
  
  // Ability modifiers - how ability affects on player/enemy
  private $modifiers = array();
  
  // Pushing here data that will be returned as js settings
  private $js_data = array();
  
  
  public function __construct($id, $title, $type, $school, $icon, $callback, $arguments, $modifiers, $data) {
    $this->id = $id;
    $this->title = $title;
    $this->type = $type;
    $this->icon = $icon;
    $this->callback = $callback;
    $this->arguments = $arguments;
    $this->modifiers = $modifiers;
    $this->school = $school;
    $this->data = $data;
  }

  
  public function applyModifiers($modifiers, $player) {
    $class_data = player_get_class_class_data($player->class);
    $stats = $player->stats;
     
    switch ($this->type) {
      case 'move':
        // First trying to apply stats modifiers
        // Every 10 speed allows to move for 1 cell further
        $improvement = floor($stats->speed / 10);
        
        $this->data['range'] = $this->data['range'] + $improvement;
        if (isset($class_data['data']['class_bonuses']['move'])) {
          foreach ($class_data['data']['class_bonuses']['move'] as $bonus) {
            if (!isset($this->data['bonus'][$bonus])) {
              $this->data['bonus'][$bonus] = TRUE;
            }
          }
        }
        break;
      
      case 'attack':
        // Every 10 strength gives +10% weapon damage

        break;
    }
    
    if ($modifiers) {
      
    }
  }
  
  public function generateJsSettings() {
    $ret = array(
      'data' => array(
        'd_duration' => $this->data['duration'],
        'duration' => $this->duration,
        'd_cooldown' => $this->data['cooldown'],
        'cooldown' => $this->cooldown,
      ),
      'arguments' => $this->arguments,
    );
    
    if (isset($this->data['range'])) {
      $ret['data']['range'] = $this->data['range'];
    }
    
    if (isset($this->data['bonus'])) {
      $ret['data']['bonus'] = $this->data['bonus'];
    }
    
    return $ret;
  }
  
  public function is_active() {
    return $this->active;
  }
  
  public function is_cooldowning() {
    return $this->cooldown > 0;
  }
  
  public function implement($arguments, &$battle, $player) {
    if ($this->active) {
      throw new Exception('Ability is already in use. You cannot implement active abilities');
    }
    if ($this->cooldown) {
      throw new Exception('Ability cooldowning');
    }
    $this->active = TRUE;
    $this->duration = $this->data['duration'];
    
    $callback = $this->callback;
    $callback($arguments, $battle, $this->data, $player);
  }
  
  public function stepForward() {
    if ($this->duration) {
      $this->duration--;
      
      if (!$this->duration) {
        $this->active = FALSE;
        $this->cooldown = $this->data['cooldown'];
      }
    }
    
    if ($this->cooldown) {
      $this->cooldown--;
    }
    
  }
  
  public function renew() {
    if ($this->active) {
      $this->duration = $this->data['duration'];
    }
  } 
}