<?php

/**
 * Playable entity class. All players should implement this 
 */
interface pEntity {
  public function getPos();
  public function setPos($x, $y);
}
