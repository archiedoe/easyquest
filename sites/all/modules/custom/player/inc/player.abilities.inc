<?php

include_once 'player.ability.php.inc';

/**
 * Describing all abilities here
 * 
 * @return array
 */
function player_define_abilities() {
  $abilities = array();

  /**
   * Walk ability
   */
  $icon = drupal_get_path('module', 'player') . '/inc/ability_icons/walk.png';
  $arguments = array(
    'pos' => array(
      'count' => 1,
    ),
  );
  $modifiers = array(
    'self' => array(
      'pos' => array(
        'type' => 'pos',
        'default' => 1,
      ),
    ),
  );
  $data = array(
    'range' => 1,
    'mp' => 0,
    'cooldown' => 0,
    'duration' => 1,
    'help' => 'Now select cell to go',
  );
  $abilities[1] = new Ability(1, 'Walk', 'move', 'physical', $icon, 'player_abilities_walk', $arguments, $modifiers, $data);

  /**
   * Walk ability
   */
  $icon = drupal_get_path('module', 'player') . '/inc/ability_icons/attack.png';
  $arguments = array(
    'pos' => array(
      'count' => 1,
    ),
  );
  $modifiers = array(
    'enemy' => array(
      'hp' => array(
        'type' => 'hp',
        'default' => 1,
      ),
    ),
    'self' => array(
      'pos' => array(
        'type' => 'pos',
        'default' => 1,
      ),
    ),
  );
  $data = array(
    'range' => 1,
    'mp' => 0,
    'cooldown' => 0,
    'duration' => 1,
    'help' => 'Now select cell to attack',
  );
  $abilities[2] = new Ability(2, 'Attack', 'attack', 'physical', $icon, 'player_abilities_attack', $arguments, $modifiers, $data);
  
  /**
   * Double speed buff
   */
  $icon = drupal_get_path('module', 'player') . '/inc/ability_icons/walk.png';
  $arguments = array();
  $modifiers = array(
    'self' => array(
      'speed' => array(
        'type' => 'buff',
        'default' => 1,
      ),
    ),
  );
  $data = array(
    'mp' => 0,
    'cooldown' => 0,
    'duration' => 1,
    'help' => 'Now select cell to go',
  );
  $abilities[3] = new Ability(3, 'Double speed', 'buff', 'physical', $icon, '', $arguments, $modifiers, $data);

  return $abilities;
}

// Getting ability by id
function player_get_ability($id) {
  $ab = player_define_abilities();

  return $ab[$id];
}

// Getting abilities by ids
function player_get_abilities($ids) {
  $ab = player_define_abilities();


  $filtered = array();
  foreach ($ids as $id) {
    $filtered[$id] = $ab[$id];
  }
  return $filtered;
}

/**
 * 
 * @param type $arguments arguments that came from JS
 * @param type $battle main battle object
 * @param type $data ability additional information - with current bufs/debufs
 * @param type $pid player id (1 or 2)
 * @throws Exception
 */
function player_abilities_walk($arguments, &$battle, &$data, $pid) {
  if (isset($arguments['pos'])) {
    $class_data = player_get_class_class_data($battle->{'p' . $pid}->original_player->class);

    // If class has no ability cornerwalker, user may walk only 
    // by x or y axis at once
    // Cornerwalker
    $dx = abs($arguments['pos'][0]['x'] - $battle->{'p' . $pid}->pos->x);
    $dy = abs($arguments['pos'][0]['y'] - $battle->{'p' . $pid}->pos->y);
    $range = $data['range'];

    $cornerwaleker = isset($class_data['data']['class_bonuses']['move']['corner']) && $dx <= $range && $dy <= $range;
    $onedierctional = ($dx <= $range && $dy == 0) || ($dx == 0 && $dy <= $range);

    if ($onedierctional || $cornerwaleker) {
      $battle->buttleGround[$battle->{'p' . $pid}->pos->x][$battle->{'p' . $pid}->pos->y] = NULL;
      $battle->buttleGround[$arguments['pos'][0]['x']][$arguments['pos'][0]['y']] = $battle->{'p' . $pid};
      $battle->{'p' . $pid}->pos->x = $arguments['pos'][0]['x'];
      $battle->{'p' . $pid}->pos->y = $arguments['pos'][0]['y'];
    } 
    else {
      throw new Exception('Illegal choice while walking');
    }
  } else {
    throw new Exception('Position not set.');
  }
}

function player_abilities_attack() {
  
}
