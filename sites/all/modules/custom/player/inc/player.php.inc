<?php

class Player {
  static private $db_name = 'player';
  public $uid = 0;
  public $level = 0;
  public $stats = array();
  public $class = 0;
  public $inventory = array();
  public $equipment = array();
  public $data = array();
  private $abilities = array();
  private $new = FALSE;
  
  public function __construct($uid, $new = FALSE) {
    $this->new = $new;
    $this->uid = $uid;
    if ($uid !== NULL && !$new) { 
      $this->uid = $uid;
      $this->load();
    }
    elseif (!$uid) {
      throw new Exception('Empty user initialize');
    }
  }
  
  public function load() {
    if (!$this->uid) {
      global $user;
      $this->uid = $user->uid;
    }
    $item = db_query('SELECT * FROM {' . Player::$db_name . '} WHERE uid = :uid', array(':uid' => $this->uid))->fetchObject();
    $this->init($item);
  }
  
  public function save() {
    if (!$this->new) {
      $this->update();
    }
    else {
      $this->insert();
    }
  }
  
  private function insert() {
    $data = $this->createDatabaseArr();
    $data['uid'] = $this->uid;
    
    $this->eid = db_insert(Player::$db_name)->fields($data)->execute();
  }
  
  private function update() {
    $data = $this->createDatabaseArr();
    db_update(Player::$db_name)
      ->fields($data)
      ->condition('uid', $this->uid)
      ->execute();
  }
  
  public function init($data) {
    $item = is_object($data) ? $data : (object) $data;
    
    $this->uid = $item->uid;
    $this->level = $item->level;
    $this->class = $item->class;
    $this->stats = is_string($item->stats) ? json_decode($item->stats) : $item->stats;
    $this->inventory = is_string($item->inventory) ? json_decode($item->inventory) : $item->inventory;
    $this->equipment = is_string($item->equipment) ? json_decode($item->equipment) : $item->equipment;
    $this->abilities = is_string($item->abilities) ? json_decode($item->abilities) : $item->abilities;
    $this->data = is_string($item->data) ? json_decode($item->data) : $item->data;
    
    // Initializing equipment
    foreach ($this->equipment as &$eq) {
      if ($eq && $eq != -1) {
        $eq = new Equip($eq);
      }
    }
  }
  
  public function initialized() {
    return $this->uid !== NULL;
  }
  
  /**
   * Creates array form current object, that contains only in-game needed properties
   */
  public function createArr() {
    $data = array();
    
    $data['level'] = $this->level;
    $data['stats'] = $this->stats;
    $data['class'] = $this->class;
    $data['inventory'] = $this->inventory;
    $data['equipment'] = $this->equipment;
    $data['level'] = $this->level;
    $data['data'] = $this->data;
    
    return $data;
  }
  
  public function createDatabaseArr() {
    $data = array();
    
    $data['level'] = $this->level;
    $data['stats'] = json_encode($this->stats);
    $data['class'] = $this->class;
    $data['inventory'] = json_encode($this->inventory);
    $data['equipment'] = json_encode($this->equipment);
    $data['level'] = $this->level;
    $data['data'] = json_encode($this->data);
    
    return $data;
  }
  
  public function getAbilities() {
    return $this->abilities->active;
  }
}
