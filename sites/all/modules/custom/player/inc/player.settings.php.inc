<?php

function player_get_settings() {
  
  $default_class_data = array(
    PLAYER_CLASS_WARRIOR => array(
      'stats' => array(
        'agility' => 1,
        'strength' => 2,
        'intelligence' => 0,
        'hp' => 80,
        'mp' => 20,
        'defence' => 5,
        'speed' => 3,
      ),
      'equip' => array(
        1 => 1, // Left hand
        2 => -1, // Right hand
        3 => 0, // Head
        4 => -1, // Chest
        5 => -1, // Legs
        6 => 0, // Acessory
      ),
      'data' => array(
        'class_bonuses' => array(
          'move' => array(
            'corner' => 'corner',
          ),
        ),
      ),
    ),
    PLAYER_CLASS_MAGICIAN => array(
      'stats' => array(
        'agility' => 1,
        'strength' => 0,
        'intelligence' => 2,
        'hp' => 60,
        'mp' => 40,
        'defence' => 2,
        'speed' => 4,
      ),
      'equip' => array(
        1 => 0,
        2 => 0,
        3 => 0,
        4 => -1,
        5 => -1,
        6 => 0,
      ),
      'data' => array(
        'class_bonuses' => array(
          'move' => array(
            'corner' => 'corner',
          ),
        ),
      ),
    ),
    PLAYER_CLASS_ROGUE => array(
      'stats' => array(
        'agility' => 2,
        'strength' => 1,
        'intelligence' => 0,
        'hp' => 70,
        'mp' => 30,
        'defence' => 3,
        'speed' => 5,
      ),
      'equip' => array(
        1 => 0,
        2 => 0,
        3 => 0,
        4 => 0,
        5 => 0,
        6 => -1,
      ),
      'data' => array(
        'class_bonuses' => array(
          'move' => array(
            'corner' => 'corner',
          ),
        ),
      ),
    ),
  );
  
  return $default_class_data;
}

function player_get_class_name($id) {
  switch ($id) {
    case 1:
      return 'warrior';
      break;
    case 2:
      return 'rogue';
      break;
    case 3:
      return 'wizard';
      break;
  }
}

function player_get_class_class_data($class_id) {
  $data = player_get_settings();
  
  return $data[$class_id];
}