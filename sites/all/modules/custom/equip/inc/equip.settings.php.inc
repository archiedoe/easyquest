<?php

function equip_get_settings() {
  $settings = array();
  
  $settings['type'] = array(
    1 => t('Weapon'),
    2 => t('Wear'),
    3 => t('Consumable'),
  );
  
  $settings['sub_type'] = array(
    1 => t('Sword'),
    2 => t('Staff'),
    3 => t('Dagger'),
    4 => t('Fabric'),
    5 => t('Leather'),
    6 => t('Mail'),
  );
  
  $settings['slot'] = array(
    1 => t('Head'),
    2 => t('Chest'),
    3 => t('Legs'),
    4 => t('Left Hand'),
    5 => t('Right Hand'),
    6 => t('Two Hand'),
  );
  
  return $settings;
}

