<?php

class Equip {
  static private $db_name = 'equip';
  public $eid;
  public $title = '';
  public $description = '';
  public $image = 0;
  public $type = 0;
  public $level = 0;
  public $sub_type = 0;
  public $slot = 0;
  public $strength = 0;
  public $agility = 0;
  public $intelligence = 0;
  public $defence = 0;
  public $hp = 0;
  public $mp = 0;
  public $min_damage = 0;
  public $max_damage = 0;
  public $additional = '';
  public $speed = 0;
  public $att_speed = 0;
  
  public function __construct($eid = NULL) {
    $this->eid = $eid;
    $this->load();
  }
  
  public function load() {
    if ($this->eid) {
      $item = db_query('SELECT * FROM {' . Equip::$db_name . '} WHERE eid = :eid', array(':eid' => $this->eid))->fetchObject();
      $this->init($item);
    }
    else {
      throw new Exception('Equip load without initialized');
    }
  }
  
  public function save() {
    if (isset($this->eid)) {
      $this->update();
    }
    else {
      $this->insert();
    }
    
  }
  
  private function insert() {
    $data = $this->createArr();
    $this->eid = db_insert(Equip::$db_name)->fields($data)->execute();
  }
  
  private function update() {
    $data = $this->createArr();
    db_update(Equip::$db_name)
      ->fields($data)
      ->condition('eid', $this->eid)
      ->execute();
  }
  
  public function init($data) {
    $item = is_object($data) ? $data : (object) $data;
    $image = file_load($item->image);
    
    $this->title = $item->title;
    $this->description = $item->description;
    $this->image = file_create_url($image->uri);
    $this->type = $item->type;
    $this->level = $item->level;
    $this->sub_type = $item->sub_type;
    $this->slot = $item->slot;
    $this->strength = $item->strength;
    $this->agility = $item->agility;
    $this->intelligence = $item->intelligence;
    $this->defence = $item->defence;
    $this->hp = $item->hp;
    $this->mp = $item->mp;
    $this->min_damage = $item->min_damage;
    $this->max_damage = $item->max_damage;
    $this->additional = $item->additional;
    $this->speed = $item->speed;
    $this->att_speed = $item->att_speed;
  }
  
  public function initialized() {
    return $this->eid !== NULL;
  }
  
  /**
   * Creates array form current object, that contains only in-game needed properties
   */
  public function createArr() {
    $data = array();
    
    $data['title'] = $this->title;
    $data['description'] = $this->description;
    $data['image'] = $this->image;
    $data['type'] = $this->type;
    $data['level'] = $this->level;
    $data['sub_type'] = $this->sub_type;
    $data['slot'] = $this->slot;
    $data['strength'] = $this->strength;
    $data['agility'] = $this->agility;
    $data['intelligence'] = $this->intelligence;
    $data['defence'] = $this->defence;
    $data['hp'] = $this->hp;
    $data['mp'] = $this->mp;
    $data['min_damage'] = $this->min_damage;
    $data['max_damage'] = $this->max_damage;
    $data['additional'] = $this->additional;
    $data['speed'] = $this->speed;
    $data['att_speed'] = $this->att_speed;
    
    return $data;
  }
}
