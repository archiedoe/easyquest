<?php

function equip_add_edit_equip_form($form, &$form_state, $item = NULL) {
  $form = array();
  $settings = equip_get_settings();
  
  if (!isset($item)) {
    $item = new Equip();
  }
  $form['#item'] = $item;
  
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => $item->title,
  );
  
  if ($item->image) {
    $file = file_load($item->image);
    
    $form['image_preview'] = array(
      '#markup' => theme('image', array('path' => file_create_url($file->uri))),
    );
  }
  
  $form['image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Image'),
    '#default_value' => $item->image,
    '#upload_location' => 'public://equip/',
  );
  
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => $item->description,
  );
  
  $form['level'] = array(
    '#title' => t('Level'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->level,
  );
  
  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $settings['type'],
    '#default_value' => $item->type,
  );
  
  $form['sub_type'] = array(
    '#title' => t('Sub Type'),
    '#type' => 'select',
    '#options' => $settings['sub_type'],
    '#default_value' => $item->sub_type,
  );
  
  $form['slot'] = array(
    '#title' => t('Slot'),
    '#type' => 'select',
    '#options' => $settings['slot'],
    '#default_value' => $item->slot,
  );
  
  $form['stats'] = array(
    '#title' => t('Stats'),
    '#type' => 'fieldset',
  );
  
  $form['stats']['hp'] = array(
    '#title' => t('HP'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->hp,
  );
  
  $form['stats']['mp'] = array(
    '#title' => t('MP'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->mp,
  );
  
  $form['stats']['strength'] = array(
    '#title' => t('Strength'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->strength,
  );
  
  $form['stats']['agility'] = array(
    '#title' => t('Agility'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->agility,
  );
  
  $form['stats']['intelligence'] = array(
    '#title' => t('Intellgence'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->intelligence,
  );
  
  $form['stats']['defence'] = array(
    '#title' => t('Defence'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->defence,
  );
  
  $form['stats']['min_damage'] = array(
    '#title' => t('Min Damage'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->min_damage,
  );
  
  $form['stats']['max_damage'] = array(
    '#title' => t('Max Damage'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->max_damage
  );
  
  $form['stats']['speed'] = array(
    '#title' => t('Speed'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->speed,
  );
  
  $form['stats']['att_speed'] = array(
    '#title' => t('Attack Speed'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $item->att_speed,
  );
  
  $form['additional'] = array(
    '#title' => t('Additional'),
    '#type' => 'textarea',
    '#default_value' => $item->additional,
  );
  
  $form['actions'] = array();
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function equip_add_edit_equip_form_submit($form, &$form_state) {
  if ($form_state['values']['image']) {
    $file = file_load($form_state['values']['image']);
    $file->status = FILE_STATUS_PERMANENT;
  }
  
  $data = $form_state['values'];
  unset($data['submit'], $data['form_build_id'], $data['form_token'], $data['form_id'], $data['op']);
  
  
  $form['#item']->init($data);
  $form['#item']->save();
  
  if ($form_state['values']['image']) {
    $file = file_load($form_state['values']['image']);
    $file->status = FILE_STATUS_PERMANENT;
    file_usage_add($file, 'equip', 'equip', $form['#item']->eid);
  }
}