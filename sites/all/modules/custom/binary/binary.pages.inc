<?php

function binary_main_page() {
  return views_embed_view('binarry', 'panel_pane_1');
}

function binary_start($binary) {
  global $user;

  // Creating new binary
  $data = array('generated' => array(), 'selected' => array());
//  $type = binary_get_type($binary);
//  $type_data = unserialize($type->data);
  // We generating results before user choise. That's right.
  for ($i = 0; $i < count($binary->field_goods[LANGUAGE_NONE]); $i++) {
    $data['generated'][] = mt_rand(0, 1);
  }

  $record = array(
    'bid' => NULL,
    'bin_type' => $binary->nid,
    'uid' => $user->uid,
    'description' => '',
    'started' => time(),
    'status' => BINARY_GAME_STATUS_IN_PROGRESS,
    'data' => serialize($data),
  );

  drupal_write_record('binary_lotery', $record);
  drupal_goto('binary/' . $record['bid']);
}

function binary_match($id) {
  // For Future use
//  $link = array(
//    '#title' => $text,
//    '#href' => '',
//    '#type' => 'link',
//    '#options' => array('html' => TRUE),
//    '#attributes' => array('id' => $id, 'class' => $class),
//    '#ajax' => array(
//      'path' => "s-user/ajax/favorite/{$count}/{$post_id}/{$user->uid}/" . (integer) !$state,
//      'wrapper' => $id,
//      'method' => 'replace',
//    ),
//  );
  // Getting game data.
  $game = db_query('SELECT * FROM {binary_lotery} WHERE bid = :id', array(':id' => $id))->fetch();
  $data = unserialize($game->data);

  // Getting type node.
  $binary_type = node_load($game->bin_type);
  $goods = array_values(binary_get_goods($binary_type));


  $rows = array();
  foreach ($data['generated'] as $key => $item) {
    if (isset($data['selected'][$key])) {
      if ($data['selected'][$key] == $item) {
        $rows[] = array(
          0 => $item == 0 ? '<div class="correct">Верно</div>' : '',
          1 => $item == 1 ? '<div class="correct">Верно</div>' : '',
        );
      }
      else {
        $rows[] = array(
          0 => 0 == $data['selected'][$key] ? '<div class="incorrect">Неверно</div>' : '',
          1 => 1 == $data['selected'][$key] ? '<div class="incorrect">Неверно</div>' : '',
        );
      }
    }
    else {
      if ((isset($data['selected'][$key - 1]) || $key == 0) && $game->status != BINARY_GAME_STATUS_LOST) {
        $rows[] = array(
          l('Выбрать', 'binary/' . $id . '/do-coice/0'),
          l('Выбрать', 'binary/' . $id . '/do-coice/1'),
        );
      }
      else {
        $rows[] = array('', '');
      }
    }
  }

  // Adding images. Yah, I know we can do that in single run, but I wanna separate logic and graphics.
  // he-he, graphics.
  foreach ($rows as $key => &$item) {
    $path = file_create_url($goods[$key]['item']->field_image[LANGUAGE_NONE][0]['uri']);
    $item[2] = '<img src="' . $path . '"/>';
  }

  $header = '<h1>' . $binary_type->title . '</h1>';

  if ($game->status == BINARY_GAME_STATUS_LOST) {
    $header .= '<h2>К сожалению, вы проиграли. Попробуйте ' . l(t('снова'), 'binary/start/' . $game->bin_type) . '!</h2>';
  }
  elseif ($game->status == BINARY_GAME_STATUS_WON) {
    $header .= '<h2>Поздравляю! Вы выиграли! Хотите сыграть ' . l(t('снова?'), 'binary/start/' . $game->bin_type) . '!</h2>';
  }

  return $header . theme('table', array('rows' => $rows, 'attributes' => array('class' => array('binary-table'))));
}

function binary_do_choice($id, $choice) {
  global $user;

  $game = db_query('SELECT * FROM {binary_lotery} WHERE bid = :id', array(':id' => $id))->fetch();

  $data = unserialize($game->data);
  $data['selected'][] = $choice;

  $binary_type = node_load($game->bin_type);

  // Checking what to do depending on game type
  if ($binary_type->field_type[LANGUAGE_NONE][0]['value'] == 1) {
    if ($choice != $data['generated'][count($data['selected']) - 1]) {
      $status = BINARY_GAME_STATUS_LOST; // Lost game.
    }
    elseif (count($data['generated']) == count($data['selected'])) {
      $status = BINARY_GAME_STATUS_WON; // Won game.
      $goods = binary_get_goods($binary_type);


      // Saving prizes
      $processed_prices = array();
      foreach ($goods as $gid => $good) {
        $processed_prices[$gid] = $good['quantity'];
      }

      $data['prizes'] = $processed_prices;

      foreach ($goods as $good) {
        binary_take_off_balance($good['item']);
      }
    }
  }

  $to_update = array('data' => serialize($data));
  if (isset($status)) {
    $to_update['status'] = $status;
  }

  db_update('binary_lotery')
    ->fields($to_update)
    ->condition('bid', $id)
    ->execute();

  drupal_goto('binary/' . $id);
}

function binary_pay_for_games() {
  $games = binary_get_not_paid_games();
  $table_data = array();

  $prises = node_load_multiple(array(), array('type' => 'goods'));
  $prise_images = array();
  foreach ($prises as $nid => $prise) {
    $prise_images[$nid] = file_create_url($prise->field_image[LANGUAGE_NONE][0]['uri']);
  }

  $field = field_info_field('field_server_name');
  foreach ($games as $game) {
    $p = '';
    foreach ($game->data['prizes'] as $id => $quantity) {
      $p .= '<img src="' . $prise_images[$id] . '"/>' . ' x ' . $quantity . '<br />';
    }

    $table_data[] = array(
      $game->bid,
      $game->field_rotmg_nickname_value,
      $field['settings']['allowed_values'][$game->field_server_name_value],
      date('d m Y', $game->started),
      l('Ссылка', 'binary/' . $game->bid, array('attributes' => array('target' => '_blank'))),
      $p,
      l('Выплачено', 'binary/pay-for-games/' . $game->bid),
    );
  }
  $header = array('Идентификатор игры', 'Ник', 'Сервер', 'Когда сыграна', 'Ссылка', 'Призы', 'Пометить как выплаченая');
  $output = theme('table', array('rows' => $table_data, 'header' => $header));

  return $output;
}

function binary_game_mark_paid($bid) {
  $query = 'UPDATE {binary_lotery} SET status = :status WHERE bid = :id';
  db_query($query, array(':status' => BINARY_GAME_STATUS_DELIVERED, ':id' => $bid));

  drupal_goto('binary/pay-for-games');
}

function binary_add_prises_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'binary', 'binary.pages');
  
  $form['#title'] = '<h3>' . t('Управление призами') . '</h3>';
  
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'binary') . '/js/script.js',
  );

  $prises = node_load_multiple([], ['type' => 'goods']);

  $form['prises'] = [
    '#tree' => TRUE,
    '#suffix' => '<div class="clearfix"></div>',
  ];

  foreach ($prises as $prise) {
    $form['prises'][$prise->nid] = [
      '#prefix' => '<div class="prise-item">',
      '#suffix' => '</div>',
    ];
    
    $form['prises'][$prise->nid]['content'] = [
      '#markup' => '<img src="' . file_create_url($prise->field_image[LANGUAGE_NONE][0]['uri']) . '"/>',
    ];
    
    $form['prises'][$prise->nid]['bottom'] = [
      '#prefix' => '<div class="prise--actions">',
      '#suffix' => '<div class="clearfix"></div></div>',
    ];
    
    $quantity = $prise->field_quantity ? $prise->field_quantity[LANGUAGE_NONE][0]['value']: 0;
    
    $form['prises'][$prise->nid]['bottom']['remove'] = [
      '#attributes' => [
        'class' => ['prise--action', 'prise--action--remove'],
      ],
      '#name' => 'prise--remove--' . $prise->nid,
      '#prise_id' => $prise->nid,
      '#type' => 'button',
      '#value' => '-',
      '#ajax' => [
        'callback' => 'binary_remove_prises_item_callback',
        'wrapper' => 'price-' . $prise->nid . '-quantity',
        'progress' => [
          'type' => 'none',
        ]
      ],
    ];
    
    $form['prises'][$prise->nid]['bottom']['quantity'] = [
      '#markup' => '<span id="price-' . $prise->nid . '-quantity" class="prise--quantity">' . $quantity . '</span>',
    ];
    
    $form['prises'][$prise->nid]['bottom']['add'] = [
      '#attributes' => [
        'class' => ['prise--action', 'prise--action--add'],
      ],
      '#name' => 'prise--add--' . $prise->nid,
      '#prise_id' => $prise->nid,
      '#type' => 'button',
      '#value' => '+',
      '#ajax' => [
        'callback' => 'binary_add_prises_item_callback',
        'wrapper' => 'price-' . $prise->nid . '-quantity',
        'progress' => [
          'type' => 'none',
        ]
      ],
    ];
  }



  $form['actions']['#type'] = 'actions';

  return $form;
}

function binary_remove_prises_item_callback($form, &$form_state) {
  $prise = node_load($form_state['triggering_element']['#prise_id']);
  $quantity = $prise->field_quantity ? $prise->field_quantity[LANGUAGE_NONE][0]['value']: 0;
  
  if (!$quantity) {
    return;
  }
  else {
    $prise->field_quantity[LANGUAGE_NONE][0]['value'] = --$quantity;
    node_save($prise);
  }
  
  return '<span id="price-' . $prise->nid . '-quantity" class="prise--quantity">' . $quantity . '</span>';
}

function binary_add_prises_item_callback($form, &$form_state) {
  $prise = node_load($form_state['triggering_element']['#prise_id']);
  $quantity = $prise->field_quantity ? $prise->field_quantity[LANGUAGE_NONE][0]['value']: 0;
  
  $prise->field_quantity[LANGUAGE_NONE][0]['value'] = ++$quantity;
  node_save($prise);
  
  return '<span id="price-' . $prise->nid . '-quantity" class="prise--quantity">' . $quantity . '</span>';
}

function binary_add_prises_form_submit($form, &$form_state) {
  binary_add_to_balance($form_state['values']['prises'], $form_state['values']['amount']);
}
