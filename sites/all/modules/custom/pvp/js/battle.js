// Declaring some global variables
var battle = {
  currentAction: null,
  currentActionId: 0,
  awaitingAction: false,
  started: false
};

(function($) {
  function activateHandlers() {
    $('.user-commands .ability-link:not(.inactive)').click(function() {
      battle.awaitingAction = true;
      $('.ability-link').removeClass('active');
      $(this).addClass('active');
      var abilityId = $(this).attr('id').split('-')[1];

      var ability_data = settings.pvp.abilities[abilityId];

      if (ability_data.data.help !== undefined) {
        $('.player-message').text(ability_data.data.help);
      }
      if (ability_data.arguments !== undefined) {
        if (ability_data.arguments.pos !== undefined) {
          highlightPositions(ability_data.data, settings.pvp.p_data);
        }
        battle.currentAction = ability_data;
        battle.currentActionId = abilityId;
      }
    });


    $('.battle .cell:not(.active)').click(function() {
      if (battle.currentAction !== null && battle.currentAction.arguments !== undefined && battle.currentAction.arguments.pos !== undefined) {
        $(this).addClass('active').css('background', '#22ff44').css('opacity', '0.3');

        var pos = $(this).attr('id').split('-');
        if (battle.currentAction.arguments.pos.data === undefined) {
          battle.currentAction.arguments.pos.data = [];
        }

        battle.currentAction.arguments.pos.data.push({x: pos[1], y: pos[2]});
        battle.currentAction.arguments.pos.count--;

        if (battle.currentAction.arguments.pos.count === 0) {

          var data = {
            id: battle.currentActionId,
            arguments: {
              pos: battle.currentAction.arguments.pos.data
            }
          };
          battle.currentAction = {};
          battle.currentActionId = 0;
          battle.awaitingAction = false;
          implementAction(data);
        }

      }
    });
  }

  function implementAction(data) {
    $.ajax({
      url: '/battle/do-action',
      type: 'POST',
      data: data,
      success: function(data) {
        if (data.error === undefined) {
          settings = data.settings;
          $('.game-board').html(data.html);
          activateHandlers();
        }
        else {
          $('.player-message').text(data.error);
        }
      }
    });
  }
  ;

  function highlightPositions(data, pdata) {
    $('.cell').removeClass('to-go');
    var range = data.range;

    var startX = pdata.pos.x - range >= 0 ? pdata.pos.x - range : 0;
    var startY = pdata.pos.y - range >= 0 ? pdata.pos.y - range : 0;
    var finishX = parseInt(pdata.pos.x) + range <= 6 ? parseInt(pdata.pos.x) + range : 6;
    var finishY = parseInt(pdata.pos.y) + range <= 4 ? parseInt(pdata.pos.y) + range : 4;
    
    
    if (data.bonus !== undefined && data.bonus.corner !== undefined) {
      for (var i = startX; i <= finishX; i++) {
        for (var j = startY; j <= finishY; j++) {
          if (i != pdata.pos.x || j != pdata.pos.y) {
            $('#cell-' + i + '-' + j).addClass('to-go');
          }
        }
      }
    }
    else {
      for (var i = startX; i <= finishX; i++) {
        if (i != pdata.pos.x) {
          $('#cell-' + i + '-' + pdata.pos.y).addClass('to-go');
        }
      }
      for (var j = startY; j <= finishY; j++) {
        if (j != pdata.pos.y) {
          $('#cell-' + pdata.pos.x + '-' + j).addClass('to-go');
        }
      }
    }
  }

  $(document).ready(function() {
    activateHandlers();

    if (!battle.started) {
      battle.started = true;
      setInterval(function() {
        if (!battle.awaitingAction) {
          $.ajax({
            url: '/battle/update',
            type: 'POST',
            success: function(data) {
              if (!battle.awaitingAction) {
                settings = data.settings;
                $('.game-board').html(data.html);
                activateHandlers();
              }
            }
          });
        }
      }, 1000);
    }
  });

})(jQuery);


