(function($) {
  
  var battleReady = false;
  (function worker() {
    $.ajax({
      url: '/battle/check-status',
      success: function(data) {
        if (data === 'ready') {
          battleReady = true;
          window.location = '/battle';
        }
        $('.battle-status').html(data);
      },
      complete: function() {
        if (!battleReady) {
          setTimeout(worker, 1000);
        }
      }
    });
  })();
})(jQuery);