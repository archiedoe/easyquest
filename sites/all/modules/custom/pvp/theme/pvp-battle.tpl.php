<div class="game-board" id="game-board">
  <div class="player-message-wrapper">
    <div class="player-message"></div>
  </div>
  <div class="battle-wrapper">
    <div class="battle">
      <div class="characters" id="characters">
        <?php if (isset($battle->p1)): ?>
          <div id="player1" class="player1 <?php print player_get_class_name($battle->p1->original_player->class); ?>"
               style="left:<?php print $battle->p1->pos->x * 100; ?>px; top:<?php print $battle->p1->pos->y * 100; ?>px;"></div>
        <?php endif; ?>
        <?php if (isset($battle->p2)): ?>
          <div id="player2" class="player2 <?php print player_get_class_name($battle->p2->original_player->class); ?>"
               style="left:<?php print $battle->p2->pos->x * 100; ?>px; top:<?php print $battle->p2->pos->y * 100; ?>px;"></div>
        <?php endif; ?>
      </div>
      
      <?php for ($i = 0; $i <= 4; $i++): ?>
        <?php for ($j = 0; $j <= 6; $j++): ?>
          <div id="<?php print 'cell-' . $j . '-' . $i; ?>"class="cell <?php print 'row' . $i . ' ' . 'col' . $j; ?>" style="top:<?php print $i * 100; ?>px; left:<?php print $j * 100; ?>px;"></div>
        <?php endfor; ?>
      <?php endfor; ?>
    </div>
  </div>
  <div class ="user-commands-wrapper">
    <div class="user-commands">
      <?php if (!$abilities['active']): ?>
        <div class="inactive"></div>
      <?php endif; ?>
      <?php foreach ($abilities['abilities'] as $key => $ability): ?>
        <div class="ability">
          <a class="ability-link" id="<?php print 'ability-' . $key; ?>" ><img src="/<?php print $ability->icon; ?>"  alt="<?php print $ability->title; ?>"/></a>
        </div>
      <?php endforeach; ?>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
