<?php

class Battle {
  static private $db_name = 'pvp_battle_data';
  
  // Players data
  public $p1;
  public $p2;
  
  public $p1_uid;
  public $p2_uid;
  
  // Battle data
  public $state = '';
  public $bid = 0;
  public $started = 0;
  public $finished = 0;
  
  private $curr_step;
  private $turn = 1;
  
  public $buttleGround;
  
  public function __construct($uid = NULL, $bid = NULL) {
    if (isset($uid)) { 
      if ($this->in_battle($uid)) {
        $this->load($uid);
      }
      else {
        $this->new_battle($uid);
      }
    }
    elseif (isset($bid)) {
      $this->load_by_bid($bid);
    }
  }
  
  public function join($uid) {
    if (!$this->p2_uid) {
      $this->p2_uid = $uid;
    
      $this->create();
      $this->save();
    }
    else {
      throw new Exception('Battle is full');
    }
  }
  
  private function load($uid) {
    $item = db_query('SELECT * FROM {' . Battle::$db_name . '} WHERE p1 = :uid OR p2 = :uid AND finished = 0',
      array(':uid' => $uid))->fetchObject();
    
    $this->init($item);
  }
  
  private function load_by_bid($bid) {
    $item = db_query('SELECT * FROM {' . Battle::$db_name . '} WHERE bid = :bid', array(':bid' => $bid))->fetchObject();
    
    $this->init($item);
  }
  
  private function new_battle($uid) {
    $this->p1_uid = $uid;
    $this->state = 'awaiting participant';
    
    $data = $this->prepare_data();
    
    $this->insert($data);
  }
  
  public function save() {
    if ($this->bid) {
      $data = $this->prepare_data();
      $this->update($data);
    }
    else {
      $data = $this->prepare_data();
      $this->insert($data);
    }
  }
  
  public function insert($data) {
    $this->bid = db_insert(Battle::$db_name)->fields($data)->execute();
  }
  
  public function update($data) {
    db_update(Battle::$db_name)
      ->fields($data)
      ->condition('bid', $this->bid)
      ->execute();
  }
  
  public function prepare_data() {
    
    $data = array(
      'started' => $this->started,
      'finished' => $this->finished,
      'p1' => $this->p1_uid,
      'p2' => $this->p2_uid,
      'data' => serialize($this),
    );
    
    return $data;
  }
  
  public function init($data) {
    $item = is_object($data) ? $data : (object) $data;
    
    if ($item->data) {
      $this->copy(unserialize($item->data));
    }
    
    $this->bid = $item->bid;
  }
  
  public function create() {
    $this->p1 = new PlayerBattleData(new Player($this->p1_uid), new Pair(0, 2));
    $this->p2 = new PlayerBattleData(new Player($this->p2_uid), new Pair(6, 2));
    
    $this->buttleGround[0][2] = $this->p1;
    $this->buttleGround[6][2] = $this->p2;
    
    $this->state = 'ready';
    $this->curr_step = 0;
  }
  
  public function copy($item) {
    foreach ($item as $key => $value) {
      $this->{$key} = $value;
    }
  }
  
  private function in_battle($uid) {
    return db_query('SELECT 1 FROM {' . Battle::$db_name . '} where (p1 = :uid OR p2 = :uid) AND finished = 0', array(':uid' => $uid))->fetchField();
  }
  
  public function getUserAbilities($uid) {
    if ($uid == $this->p1_uid) {
      return array(
        'abilities' => $this->p1->getAbilities(),
        'js_abilities' => $this->p1->getJSAbilities(),
        'active' => $this->turn == 1,
      );
    }
    elseif ($uid == $this->p2_uid) {
      return array(
        'abilities' => $this->p2->getAbilities(),
        'js_abilities' => $this->p2->getJSAbilities(),
        'active' => $this->turn == 2,
      );
    }
    else {
      throw new Exception('Wrong uid while getting abilities');
    }
  }
  
  public function doAction($uid, $action, $arguments) {
    if ($uid == $this->p1_uid && $this->turn == 1) {
      $abilities = $this->p1->getAbilities(); 
      
      if (isset($abilities[$action])) {
        $abilities[$action]->implement($arguments, $this, 1);
        $this->p1->incrementAbilities();
      }
      else {
        throw new Exception('Wrong ability. Don\'t try to hack anymore or you\'ll get banned');
      }
      $this->turn = 2;
      $this->save();
      
    }
    elseif ($uid == $this->p2_uid && $this->turn == 2) {
      $abilities = $this->p2->getAbilities(); 
      
      if (isset($abilities[$action])) {
        $abilities[$action]->implement($arguments, $this, 2);
        $this->p2->incrementAbilities();
      }
      else {
        throw new Exception('Wrong ability. Don\'t try to hack anymore or you\'ll get banned');
      }
      $this->turn = 1;
      $this->save();
    }
    else {
      throw new Exception('Not your turn');
    }
  }
  
  public function getCommonPlayerData($uid) {
    if ($uid == $this->p1_uid) {
      return $this->p1->getCommonPlayerData();
    }
    elseif ($uid == $this->p2_uid) {
      return $this->p2->getCommonPlayerData();
    }
    else {
      throw new Exception('Wrong uid while getting player data');
    }
  }
}

class Pair {
  public $x, $y;
  
  public function __construct($x, $y) {
    $this->x = $x;
    $this->y = $y;
  }
  
  public function setPair($x, $y) {
    $this->x = $x;
    $this->y = $y;
  }
  
  public function getPair() {
    $pair = new stdClass();
    $pair->x = $this->x;
    $pair->y = $this->y;
      
    return $pair;
  }
}

class PlayerBattleData implements pEntity {
  public $original_player;
  
  public $hp;
  public $mp;
  public $pos;
  public $abilities;
  private $modifiers = array();
  public $abi_cd;
  
  public function __construct($item, $pos) {
    $this->original_player = $item;
    $this->pos = $pos;
    $this->hp = $item->stats->hp;
    $this->mp = $item->stats->mp;
    
    $this->abilities = player_get_abilities($this->setAbilities());
  }
  
  public function getJSAbilities() {
    $js_abilities = array();
    foreach ($this->abilities as $ability) {
      $js_abilities[$ability->id] = $ability->generateJsSettings();
    }
    
    return $js_abilities;
  }
  
  public function getModifiers() {
    return $this->modifiers;
  }
  
  private function setAbilities() {
    return $this->original_player->getAbilities();
  }
  
  public function getAbilities() {
    foreach ($this->abilities as &$ability) {
      $ability->applyModifiers($this->modifiers, $this->original_player);
    }
    
    return $this->abilities;
  }
  
  public function incrementAbilities() {
    foreach ($this->abilities as &$ability) {
      if ($ability->is_active() || $ability->is_cooldowning()) {
        $ability->stepForward($this->modifiers, $this->original_player);
      }
    }
  }
  
  public function getCommonPlayerData() {
    $data = new stdClass();
    $data->hp = $this->hp;
    $data->mp = $this->mp;
    $data->pos = $this->pos;
    
    return $data;
  }
  
  public function getPos() {
    return $this->pos->getPair();
  }
  
  public function setPos($x, $y) {
    $this->pos->setPair($x, $y);
  }
}